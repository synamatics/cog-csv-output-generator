const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
module.exports = {
    entry: './src/app.js',
    output: {
      filename: 'app.js',
      path: path.resolve(__dirname, 'assets'),
    },    
    plugins: [
      new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename:'[id].css',      
    })],
    module: {
      rules: [
        {
          test: /\.(sa|sc|c)ss$/,
          use: [
            // {
            //   loader: MiniCssExtractPlugin.loader,
            //   options: {
            //     publicPath: 'assets/',
            //   },
            // },
            'style-loader',
            'css-loader',
            // 'postcss-loader',
            'sass-loader',
          ],
        },
      ],
    },
  };