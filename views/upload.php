<?php app_header() ?>
<div class="container">
    <h4 class="font-weight-light">Upload CSV</h4>
    <div class="card border-0 shadow-sm card-body">
        <form action="?action=postupload" class="form row" method="POST" enctype="multipart/form-data">
            <div class="form-group col-sm-12 col-md-4">
                <label for="type">Vendor</label>
                <select name="type" id="type" class="form-control">
                    <option value="" disabled selected>--Select--</option>
                    <option value="App\VendorCollect\VendorCatch">Catch</option>
                    <option value="App\VendorCollect\Kogan">Kogan</option>
                    <option value="App\VendorCollect\MyDeal">My Deal</option>
                    <option value="App\VendorCollect\Groupon">Groupon</option>
                </select>
            </div>
            <div class="form-group col-sm-12 col-md-4">
                <label for="file">File</label>
                <input type="file" name="file" id="file" class="form-control" accept=".csv">
            </div>
            <div class="form-group col-sm-12">
                <button type="submit" class="btn btn-sm btn-primary">Save</button>
            </div>
        </form>
    </div>
</div>
<?php app_footer();?>