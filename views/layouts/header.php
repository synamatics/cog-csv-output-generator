<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>
    <!-- <link rel="stylesheet" href="./assets/app.css"> -->
    <script src="./assets/app.js"></script>
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark border-0 shadow-sm py-0">
  <a class="navbar-brand" href="#">Navbar</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item ">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/?page=upload">Upload</a>
      </li>
    </ul>
  </div>
</nav>
<main class="py-4">
<?php if(isset($_REQUEST['message'])):?>
  <div class="alert alert-info mx-2 border-0">
    <?php echo $_REQUEST['message'];?>
  </div>
<?php endif;?>