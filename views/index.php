<?php app_header() ?>
<div class="container">
    <div class="py-4">
        <h4>Uploads</h4>
    </div>

<?php 
    use Carbon\Carbon;
    $database   = new \App\Database;
    $data       = $database->table('imports')->distinct('filename,created_at,vendor')->get();
    // echo print_r($data);
?>
<div>
<table class="table table-sm">
    <thead>
        <tr>
            <th>File</th>
            <th>Vendor</th>
            <th>Upload Date</th>
            <th>Download</th>
        </tr>
    </thead>
    <?php foreach ($data as $key):?>
        <tr>
            <td><a href="/<?php echo $key->filename;?>"><?php echo $key->filename;?></a></td>
            <td><?php echo $key->vendor;?></td>
            <td><?php echo $key->created_at->toDayDateTimeString();?></td>
            <td>
                <a href="?action=download&type=1&filename=<?php echo $key->filename;?>" class="btn btn-sm btn-primary">Download 1</a>
                <a href="?action=download&type=2&filename=<?php echo $key->filename;?>" class="btn btn-sm btn-primary">Download 2</a>
            </td>
        </tr>
    <?php endforeach;?>
</table>
</div>
</div>
<?php app_footer();?>