-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.24 - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             10.2.0.5599
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table statincotton.imports
DROP TABLE IF EXISTS `imports`;
CREATE TABLE IF NOT EXISTS `imports` (
  `id` int(6) unsigned NOT NULL AUTO_INCREMENT,
  `supplier` varchar(727) DEFAULT NULL,
  `vendor` varchar(50) DEFAULT NULL,
  `delivery_name` varchar(727) DEFAULT NULL,
  `delivery_address_1` varchar(727) DEFAULT NULL,
  `delivery_address_2` varchar(727) DEFAULT NULL,
  `delivery_suburb` varchar(727) DEFAULT NULL,
  `delivery_state` varchar(727) DEFAULT NULL,
  `delivery_postcode` varchar(727) DEFAULT NULL,
  `quantity` varchar(727) DEFAULT NULL,
  `description` varchar(727) DEFAULT NULL,
  `product_code` varchar(727) DEFAULT NULL,
  `order_id` varchar(727) DEFAULT NULL,
  `delivery_email` varchar(727) DEFAULT NULL,
  `delivery_phone` varchar(727) DEFAULT NULL,
  `order_row` varchar(727) DEFAULT NULL,
  `label_info` varchar(727) DEFAULT NULL,
  `origin_warehouse` varchar(727) DEFAULT NULL,
  `destination_country` varchar(727) DEFAULT NULL,
  `order_date` varchar(727) DEFAULT NULL,
  `web_reference` varchar(727) DEFAULT NULL,
  `item_price` varchar(727) DEFAULT NULL,
  `shipping_method` varchar(727) DEFAULT NULL,
  `filename` varchar(727) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table statincotton.imports: ~417 rows (approximately)
/*!40000 ALTER TABLE `imports` DISABLE KEYS */;
/*!40000 ALTER TABLE `imports` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
