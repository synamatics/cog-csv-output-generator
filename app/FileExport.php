<?php 
namespace App;
use League\Csv\Writer;
use Carbon\Carbon;
class FileExport 
{
    protected $fileOne = [
        "Suplier" =>"supplier",
        "DeliveryName" =>"delivery_name",
        "DeliveryAddress1" =>"delivery_address_1",
        "DeliveryAddress2" =>"delivery_address_2",
        "DeliverySuburb" =>"delivery_suburb",
        "DeliveryState" =>"delivery_state",
        "DeliveryPostCode" =>"delivery_postcode",
        "Quantity" =>"quantity",
        "Description" =>"description",
        "ProductCode" =>"product_code",
        "OrderID" =>"order_id",
        "DeliveryEmailAddress" =>"delivery_email",
        "DeliveryPhone" =>"delivery_phone",
        "OrderRow" =>"order_row",
        "LabelInfo" =>"label_info",
        "OriginWarehouse" =>"origin_warehouse",
        "DestCountry" =>"destination_country",
        "OrderDate" =>"order_date",
        "WebReference" =>"web_reference",
        "ItemPrice" =>"item_price",
        "Shipping Method" =>"shipping_method",
        
    ];
    public function generateOne($data)
    {

        
        $writer = Writer::createFromString();
        $keys   = array_keys($this->fileOne);
        $writer->insertOne($keys);
        foreach ($data as $key) {
            $final = [];
            foreach ($this->fileOne as $p => $value) {
                $final[] = $key->{$value};
            }
            $writer->insertOne($final);
        }
        $writer->setEscape('');
        $writer->output(Carbon::now().'.csv');
        die;
    }
}
