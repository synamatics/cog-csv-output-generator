<?php 
namespace App;

use App\Controllers\IndexController;
class Template
{
    public static function render()
    {
        $action = $_REQUEST['action'] ?? null;
        if($action !== null){
            $apt = new IndexController;
            return $apt->$action();
        }
        $page = $_REQUEST['page'] ?? 'index';
        include "./views/{$page}.php";

    }
}
