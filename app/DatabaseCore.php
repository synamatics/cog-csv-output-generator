<?php 
namespace App;
use App\SingleTon;

abstract class DatabaseCore
{
    protected static $host  = "localhost";
    protected static $user  = "root";
    protected static $pass  = "";
    protected static $base  = "statincotton";
    protected static $conn;
    public static function connect()
    {
        
        $host  = self::$host;
        $user  = self::$user;
        $pass  = self::$pass;
        $base  = self::$base;

        try {
            self::$conn = new \PDO("mysql:host=$host;dbname=$base", $user, $pass);
            self::$conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            return self::$conn;
        }
        catch(\PDOException $e)
        {
            echo "Connection failed: " . $e->getMessage();
        }
    }
    public static function convertResult($result)
    {
        $singleton        = new SingleTon;
        foreach ($result as $key => $value) {
            $singleton->atomic($key,$value);
        }
        return $singleton->respond();
    }
}
