<?php 
namespace App;
use Carbon\Carbon;
class SingleTon
{
    protected static $items = [];
    public static function created_at (){
        foreach (self::$items as $key => $value) {
            if(isset($value->created_at))
            self::$items[$key]->created_at = Carbon::parse(self::$items[$key]->created_at);
        }
        return new static;
    }
    public static function atomic($key,$value)
    {
        self::$items[$key] = $value;
        return new static;
    }
    public static function respond()
    {
        self::created_at();
        return (object) self::$items;
    }
}
