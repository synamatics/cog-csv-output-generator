<?php 

namespace App;

use App\DatabaseCore;
use Carbon\Carbon;

class Database extends DatabaseCore
{
    protected static $table;
    protected static $query = [];
    protected static $limit;
    protected static $distinct;

    public static function table($table)
    {
        self::$table = $table;
        return new static;
    }
    public function initializeTable()
    {
        $sql = "CREATE TABLE imports (
                id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
                supplier VARCHAR(727) NULL,
                delivery_name VARCHAR(727) NULL,
                delivery_address_1 VARCHAR(727) NULL,
                delivery_address_2 VARCHAR(727) NULL,
                delivery_suburb VARCHAR(727) NULL,
                delivery_state VARCHAR(727) NULL,
                delivery_postcode VARCHAR(727) NULL,
                quantity VARCHAR(727) NULL,
                description VARCHAR(727) NULL,
                product_code VARCHAR(727) NULL,
                order_id VARCHAR(727) NULL,
                delivery_email VARCHAR(727) NULL,
                delivery_phone VARCHAR(727) NULL,
                order_row VARCHAR(727) NULL,
                label_info VARCHAR(727) NULL,
                origin_warehouse VARCHAR(727) NULL,
                destination_country VARCHAR(727) NULL,
                order_date VARCHAR(727) NULL,
                web_reference VARCHAR(727) NULL,
                item_price VARCHAR(727) NULL,
                shipping_method VARCHAR(727) NULL,
                filename VARCHAR(727) NULL,
                created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
            )";
            $connection = self::connect();
            try {
                $connection->exec($sql);
                echo "Table imports created successfully";
            } catch (\PDOException $e) {
                echo $sql . "<br>" . $e->getMessage();
            }
    }
    public static function where($q1,$q2,$q3 = '=')
    {
        $x      = $q1.' ';
        $x     .= ($q3 !== '=') ? "$q2 '$q3":"$q3 '$q2'";
        
        self::$query[] = $x;
        return new static;
    }
    public static function get()
    {
        if(!self::$table)
        return 'You have not selected a table';
        $quewry     = self::constructQuery();
        $connection =  self::connect();
        $result = $connection->query($quewry);
        return self::convertResult($result->fetchAll($connection::FETCH_CLASS));
    }
    public static function first()
    {
        if(!self::$table)
        return 'You have not selected a table';

        $quewry     = self::constructQuery();
        $connection = self::connect();
        $result = $connection->query($quewry);
        return self::convertResult($result->fetch($connection::FETCH_OBJ));
    }
    
    public static function limit($start = 0,$end = 1)
    {
        self::$limit = "$start,$end";
        return new static;
    }
    public static function constructQuery()
    {
        $quewry = "SELECT ";
        if(self::$distinct)
            $quewry .= "DISTINCT ".self::$distinct." FROM";
        else
            $quewry .= "* ".self::$distinct." FROM";
            
        $quewry .= ' '.self::$table." ";
        if(count(self::$query) >= 1):
            $quewry .= ' WHERE ';
            $quewry .= implode(" AND ",self::$query);
        endif;
        if(self::$limit):
            $quewry .= " LIMIT ".self::$limit;
        endif;

        $quewry .= " ORDER BY created_at DESC";
        return $quewry;
    }
    public static function create(Array $data)
    {
        if(empty($data))
        return false;
        
        $connection     = self::connect();
        $ins_keys       = implode(",", array_keys($data));
        $stmt           = $connection->prepare("SELECT * FROM ".self::$table." where name = :name");
        $values         = [];
        foreach ($data as $key => $value) {
            $values[]     = $connection->quote($value);
        }
        $ins_values       = implode(",", array_values($values));
        $res = "INSERT INTO imports ($ins_keys) values ($ins_values) ";
        try {
            $connection->exec($res);
        }
        catch(PDOException $e)
        {
            echo $res . "<br>" . $e->getMessage();
        }
        return $res;

    }
    public static function distinct($id)
    {
        self::$distinct = $id;
        return new static;
    }
}
