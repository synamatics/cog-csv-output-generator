<?php 
namespace App\Controllers;
use App\Database;
use App\FileExport;
use Carbon\Carbon;
class IndexController 
{
    public function postupload()
    {
        header("Content-Type:application/json");
        $data       = $_POST['type'];
        $dbm        = new $data;
        $results    = $dbm->processedRecords($_FILES['file']);
        // $result2    = $dbm->processedRecords($_FILES['file'],true);
        // echo json_encode($results[1],JSON_PRETTY_PRINT);
        // echo json_encode($result2[1],JSON_PRETTY_PRINT);
        // test
        if(empty($results[1]->delivery_name)){
            header("Location: /?page=upload&message=failed"); 
            die;
        }
        else{
            $this->insertItems($results,$_FILES['file']);
            header("Location: /?page=upload&message=success"); 
            die;
        }
    }
    public function download()
    {
        $db         = new Database;
        $filename   = $_REQUEST['filename'];
        $data       = $db->table('imports')->where('filename',$filename)->get();
        $generate   = new FileExport;
        $generate->generateOne($data);
    }
    private function insertItems($results,$file)
    {
        $db         = new Database;
        $timeUpdated    = Carbon::now();
        $storeName = "uploads/".date('d-m-Y').'-'.uniqid().".csv";
        move_uploaded_file($file["tmp_name"], $storeName);
        foreach ($results as $key) {
            $red    = $db->create([
                "supplier"=>$key->supplier,
                "vendor"=>$key->vendor,
                "delivery_name"=>$key->delivery_name,
                "delivery_address_1"=>$key->delivery_address_1,
                "delivery_address_2"=>$key->delivery_address_2,
                "delivery_suburb"=>$key->delivery_suburb,
                "delivery_state"=>$key->delivery_state,
                "delivery_postcode"=>$key->delivery_postcode,
                "quantity"=>$key->quantity,
                "description"=>$key->description,
                "product_code"=>$key->sku,
                "order_id"=>$key->order_id,
                "delivery_email"=>$key->delivery_email,
                "delivery_phone"=>$key->delivery_phone,
                "order_row"=>$key->order_row,
                "label_info"=>$key->label_info,
                "origin_warehouse"=>$key->origin_warehouse,
                "destination_country"=>$key->destination_country,
                "order_date"=>$key->order_date,
                "web_reference"=>$key->web_reference,
                "item_price"=>$key->item_price,
                "shipping_method"=>$key->order_id,
                "filename"=>$storeName,
                "created_at"=>$timeUpdated,
            ]);
        }        
    }
}
