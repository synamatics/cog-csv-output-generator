<?php
 
 namespace App\VendorCollect;
 use League\Csv\Reader;
 use League\Csv\Statement;

 
 class MyDeal
 {
    protected $casts = [
        "Order No"=> "order_id",
        "Purchased Date"=> "order_date",
        "OriginWarehouse"=> "origin_warehouse",
        "Courier"=> "shipment_method",
        "Name"=> "delivery_name",
        "Address"=> "delivery_address_1",
        "Address2"=> "delivery_address_2",
        "Suburb"=> "delivery_suburb",
        "State"=> "delivery_state",
        "Postcode"=> "delivery_postcode",
        "DestCountry"=> "destination_country",
        "Quantity"=> "quantity",
        "Product Name"=> "description",
        "SKU"=> "sku",
        "Total Sale Price"=> "item_price",
        'Phone'=> "delivery_phone",
        'Customer Email'=> "delivery_email",
        "OrderRow"=> "order_row",
        "LabelInfo"=> "label_info",
        "WebReference"=> "web_reference",
    ];
    protected $supplier = 10012;
    protected $vendorName = 'My Deal';

    public function staticData($file)
    {
        $csv = Reader::createFromPath($file["tmp_name"], 'r');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);
        return $records;
    }
    public function processedRecords($file,$mat=false)
    {
        $data   = $this->staticData($file);
        return $mat? $this->processAllArray($data) : $this->processAll($data);

    }
    public function processAll($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processSingle($value);
        endforeach;
        return $final;
    }
    public function processAllArray($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processActivist($value);
        endforeach;
        return $final;
    }
    public function processActivist($item)
    {
        $object = [];
        foreach ($item as $kp => $p):
            $object[$kp] = $p??'';
        endforeach;
        return $item;
    }
    public function processSingle($item)
    {
        
        $object = new \stdClass;
        foreach ($this->casts as $key => $value) {
            $object->{$value} = "";
        }
        foreach ($item as $kp => $p):
            if(isset($this->casts[$kp]))
            $object->{$this->casts[$kp]} = $p??'';
        endforeach;
        $object->supplier   = $this->supplier;
        $object->vendor     = $this->vendorName;
        return $object;
    }
 }