<?php
 
 namespace App\VendorCollect;
 use League\Csv\Reader;
 use League\Csv\Statement;

 
 class Kogan
 {
    protected $casts = [
        "OrderID"=> "order_id",
        "OrderDate"=> "order_date",
        "OriginWarehouse"=> "origin_warehouse",
        "ShippingMethod"=> "shipment_method",
        "DeliveryName"=> "delivery_name",
        "DeliveryAddress1"=> "delivery_address_1",
        "DeliveryAddress2"=> "delivery_address_2",
        "DeliverySuburb"=> "delivery_suburb",
        "DeliveryState"=> "delivery_state",
        "DeliveryPostCode"=> "delivery_postcode",
        "DestCountry"=> "destination_country",
        "Quantity"=> "quantity",
        "Description"=> "description",
        "ProductCode"=> "sku",
        "ItemPrice"=> "item_price",
        'Shipping address phone'=> "delivery_phone",
        'DeliveryEmailAddress'=> "delivery_email",
        "OrderRow"=> "order_row",
        "LabelInfo"=> "label_info",
        "WebReference"=> "web_reference",
    ];
    protected $supplier = 10006;
    protected $vendorName = 'Kogan';
    public function staticData($file)
    {
        $csv = Reader::createFromPath($file["tmp_name"], 'r');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);
        return $records;
    }
    public function processedRecords($file,$mat=false)
    {
        $data   = $this->staticData($file);
        return $mat? $this->processAllArray($data) : $this->processAll($data);

    }
    public function processAll($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processSingle($value);
        endforeach;
        return $final;
    }
    public function processAllArray($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processActivist($value);
        endforeach;
        return $final;
    }
    public function processActivist($item)
    {
        $object = [];
        foreach ($item as $kp => $p):
            $object[$kp] = $p??'';
        endforeach;
        return $item;
    }
    public function processSingle($item)
    {
        
        $object = new \stdClass;
        foreach ($this->casts as $key => $value) {
            $object->{$value} = "";
        }
        foreach ($item as $kp => $p):
            if(isset($this->casts[$kp]))
            $object->{$this->casts[$kp]} = $p??'';
        endforeach;
        $object->supplier = $this->supplier;
        $object->vendor     = $this->vendorName;
        return $object;
    }
 }