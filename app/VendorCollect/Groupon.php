<?php
 
 namespace App\VendorCollect;
 use League\Csv\Reader;
 use League\Csv\Statement;

 
 class Groupon
 {
    protected $casts = [
        "fulfillment_line_item_id"=> "order_id",
        "order_date"=> "order_date",
        "OriginWarehouse"=> "origin_warehouse",
        "shipment_method_requested"=> "shipment_method",
        "shipment_address_name"=> "delivery_name",
        "shipment_address_street"=> "delivery_address_1",
        "shipment_address_street_2"=> "delivery_address_2",
        "shipment_address_city"=> "delivery_suburb",
        "shipment_address_stat"=> "delivery_state",
        "shipment_address_postal_code"=> "delivery_postcode",
        "shipment_address_country"=> "destination_country",
        "quantity_requested"=> "quantity",
        "item_name"=> "description",
        "groupon_sku"=> "sku",
        "sell_price"=> "item_price",
        'customer_phone'=> "delivery_phone",
        'Customer Email'=> "delivery_email",
        "OrderRow"=> "order_row",
        "LabelInfo"=> "label_info",
        "WebReference"=> "web_reference",
    ];
    protected $supplier = 10007;
    protected $vendorName = 'Groupon';
    public function staticData($file)
    {
        $csv = Reader::createFromPath($file["tmp_name"], 'r');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);
        return $records;
    }
    
    public function processedRecords($file,$mat=false)
    {
        $data   = $this->staticData($file);
        return $mat? $this->processAllArray($data) : $this->processAll($data);

    }
    public function processAll($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processSingle($value);
        endforeach;
        return $final;
    }
    public function processAllArray($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processActivist($value);
        endforeach;
        return $final;
    }
    public function processActivist($item)
    {
        $object = [];
        foreach ($item as $kp => $p):
            $object[$kp] = $p??'';
        endforeach;
        return $item;
    }
    public function processSingle($item)
    {
        
        $object = new \stdClass;
        foreach ($this->casts as $key => $value) {
            $object->{$value} = "";
        }
        foreach ($item as $kp => $p):
            if(isset($this->casts[$kp]))
            $object->{$this->casts[$kp]} = $p??'';
        endforeach;
        $object->supplier = $this->supplier;
        $object->vendor     = $this->vendorName;
        return $object;
    }
 }