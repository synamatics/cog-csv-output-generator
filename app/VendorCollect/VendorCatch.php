<?php
 
 namespace App\VendorCollect;
 use League\Csv\Reader;
 use League\Csv\Statement;

 
 class VendorCatch
 {
    protected $casts = [
        "Order number"=> "order_id",
        "Date created"=> "order_date",
        "3pl_warehouse_location"=> "origin_warehouse",
        "Shipping method"=> "shipment_method",
        "Shipping address first name"=> "delivery_name",
        "order_date"=> "order_date",
        "Shipping address street 1"=> "delivery_address_1",
        "Shipping address street 2"=> "delivery_address_2",
        "Shipping address city"=> "delivery_suburb",
        "Shipping address state"=> "delivery_state",
        "Shipping address zip"=> "delivery_postcode",
        "Shipping address country"=> "destination_country",
        "Quantity"=> "quantity",
        "Details"=> "description",
        "SKU"=> "sku",
        "Total order amount incl. VAT (including shipping charges)"=> "item_price",
        'Shipping address phone'=> "delivery_phone",
        'DeliveryEmail'=> "delivery_email",
        "OrderRow"=> "order_row",
        "LabelInfo"=> "label_info",
        "WebReference"=> "web_reference",
    ];
    protected $supplier = 10014;
    protected $vendorName = 'Catch';    
    public function staticData($file)
    {
        $csv = Reader::createFromPath($file["tmp_name"], 'r');
        $csv->setHeaderOffset(0);
        $stmt = (new Statement());
        $records = $stmt->process($csv);
        return $records;
    }
    public function processedRecords($file,$mat=false)
    {
        $data   = $this->staticData($file);
        return $mat? $this->processAllArray($data) : $this->processAll($data);

    }
    public function processAll($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processSingle($value);
        endforeach;
        return $final;
    }
    public function processAllArray($results)
    {
        $final = [];
        foreach ($results as $key => $value):
            $final[$key] = $this->processActivist($value);
        endforeach;
        return $final;
    }
    public function processActivist($item)
    {
        $headerKeys     = \explode(";",array_keys($item)[0]);
        $headerValues   = \explode(";",array_values($item)[0]);
        
        $object = [];
        
        foreach ($headerKeys as $kp => $p):
            // SKU 
            if($p == 'Details'):
                $object['SKU'] = $this->getSku($headerValues[$kp]);
            endif;
            $object[$headerKeys[$kp]] = $headerValues[$kp]??'';
        endforeach;
        return $object;
    }
    public function processSingle($item)
    {
        $headerKeys     = \explode(";",array_keys($item)[0]);
        $headerValues   = \explode(";",array_values($item)[0]);
        
        $object = new \stdClass;
        foreach ($this->casts as $key => $value) {
            $object->{$value} = "";
        }
        foreach ($headerKeys as $kp => $p):
            // SKU 
            if($p == 'Details'):
                $object->sku = $this->getSku($headerValues[$kp]);
            endif;
            // Name
            if($p == 'Shipping address last name'):
                $object->delivery_name .= ' '.$headerValues[$kp];
            endif;

            if(isset($this->casts[$headerKeys[$kp]]))
            $object->{$this->casts[$headerKeys[$kp]]} = $headerValues[$kp]??'';
        endforeach;
        $object->supplier   = $this->supplier;
        $object->vendor     = $this->vendorName;        
        return $object;
    }
    private function getSku($key){
        $step   =  explode("(Product SKU :",$key);
        $step2  =  explode("|",$step[1]);
        $step3  =  explode("_",$step2[0]);
        $step4  =  trim($step3[0].'_'.$step3[1]);
        return $step4;
    }
 }